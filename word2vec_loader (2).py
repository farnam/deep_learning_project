import numpy as np
import tensorflow as tf

word2vec_file = 'data/GoogleNews-vectors-negative300.bin'
embedding_dim = 300
# It is a dictionary containing index of each word in your vocabulary


def load_word2vec():
	init_embedding_w = np.random.uniform(-0.25, 0.25, (len(word_index_dict), embedding_dim))
	with open(word2vec_file, "rb") as f:
		header = f.readline()
		vocab_size, layer1_size = map(int, header.split())
		binary_len = np.dtype('float32').itemsize * layer1_size
		counter = 0
		for line in range(vocab_size):
			word = []
			while True:
				ch = f.read(1)
				if ch == ' ':
					word = ''.join(word)
					break
				if ch != '\n':
					word.append(ch)
			idx = word_index_dict[word]
			if idx != 0:
				init_embedding_w[idx] = np.fromstring( f.read(binary_len), dtype='float32')
			else:
				f.read(binary_len)
			counter += 1
			if counter % 100000 == 0:
				print()

	return init_embedding_w

init_embedding_w = load_word2vec()
